"use strict";

const express = require("express");

// Constants
const PORT = 8080;
const HOST = "0.0.0.0";

// App
const app = express();
app.get("/", (req, res) => {
  res.send("Docker com DevOps com sucesso!!!");
});

app.get("*", (req, res) => {
  res.send("Ops!!! Algo deu errado!", 404);
});

app.listen(PORT, HOST);
